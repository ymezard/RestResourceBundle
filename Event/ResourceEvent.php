<?php

namespace GI\RestResourceBundle\Event;

use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ResourceEvent
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package AppBundle\Event
 */
class ResourceEvent extends Event
{
    /**
     * @var string
     */
    private $resource;


    /**
     * @var ResourceManagerInterface
     */
    private $manager;

    /**
     * @var Request
     */
    private $request;

    /**
     * ResourceEvent constructor.
     *
     * @param                          $resource
     * @param ResourceManagerInterface $manager
     * @param Request                  $request
     */
    public function __construct($resource, ResourceManagerInterface $manager, Request $request)
    {
        $this->resource = $resource;
        $this->manager = $manager;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @return ResourceManagerInterface
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }
}
