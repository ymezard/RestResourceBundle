<?php

namespace GI\RestResourceBundle\Serializer\Constructor;

use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\VisitorInterface;

/**
 * Class ObjectConstructor
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Serializer\Constructor
 */
class ObjectConstructor implements ObjectConstructorInterface
{
    public function construct(
        VisitorInterface $visitor,
        ClassMetadata $metadata,
        $data,
        array $type,
        DeserializationContext $context
    ) {
        $className = $metadata->name;

        return new $className;
    }
}