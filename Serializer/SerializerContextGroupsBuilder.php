<?php

namespace GI\RestResourceBundle\Serializer;


use GI\RestResourceBundle\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

class SerializerContextGroupsBuilder implements SerializerContextGroupsBuilderInterface
{
    public function createFromRequest(Request $request, $subject, bool $normalization, array $extractedAttributes = null): array
    {
        return [];
    }
}