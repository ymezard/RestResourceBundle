<?php

namespace GI\RestResourceBundle\EventListener;

use FOS\RestBundle\View\View;
use Pagerfanta\Pagerfanta;
use GI\RestResourceBundle\Pagination\RangeHeaderTrait;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;


/**
 * Class PagedResultListener
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\EventListener
 */
class PagedResultListener
{
    use RangeHeaderTrait;

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $data = $event->getControllerResult();

        if (!$data instanceof Pagerfanta) {
            return;
        }

        $range = $this->getRequestRange($event->getRequest(), $data->getMaxPerPage());

        $data->setAllowOutOfRangePages(false);
        $data->setNormalizeOutOfRangePages(false);
        $data->setMaxPerPage($range->getMaxResults());
        $data->setCurrentPage($range->getCurrentPage());

        $total = $data->getNbResults();
        if ($total == 0) {
            $start = 0;
            $end = 0;
        } else {
            $start = $data->getCurrentPageOffsetStart() - 1;
            $end = $data->getCurrentPageOffsetEnd() - 1;
        }


        $statusCode = Response::HTTP_OK;
        if ($data->haveToPaginate()) {
            $statusCode = Response::HTTP_PARTIAL_CONTENT;
        }

        $response = new View($data->getIterator()->getArrayCopy(), $statusCode, [
            'Content-Range' => sprintf('%d-%d/%d', $start, $end, $total),
            'Access-Control-Expose-Headers' => 'Content-Range'
        ]);

//        $response = $event->getResponse();
//        if ($response instanceof Response) {
//            $response->setStatusCode($statusCode);
//            $response->headers->set(
//                'Content-Range',
//                sprintf('%d-%d/%d', $start, $end, $total)
//            );
//        }


        $event->setControllerResult($response);
    }
}