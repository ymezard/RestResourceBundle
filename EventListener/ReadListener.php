<?php

namespace GI\RestResourceBundle\EventListener;

use GI\RestResourceBundle\Exception\RuntimeException;
use GI\RestResourceBundle\Manager\ResourceManagerInterface;
use GI\RestResourceBundle\Util\RequestAttributesExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class ReadListener
{

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        try {
            $attributes = RequestAttributesExtractor::extractAttributes($request);
        } catch (RuntimeException $e) {
            return;
        }

        if ($request->isMethodSafe(false) || $request->isMethod(Request::METHOD_DELETE)) {
            return;
        }

        $manager = $request->attributes->get('manager');
        if (!$manager || !$manager instanceof ResourceManagerInterface) {
            return;
        }
    }
}