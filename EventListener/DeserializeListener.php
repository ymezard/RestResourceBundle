<?php

namespace GI\RestResourceBundle\EventListener;

use JMS\Serializer\SerializerInterface;

class DeserializeListener
{

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * DeserializeListener constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


}