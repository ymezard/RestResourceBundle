<?php

namespace RestBundle\EventListener;

use JMS\Serializer\Serializer;
use GI\RestResourceBundle\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class ValidationExceptionListener
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package ApiPlatform\Core\Bridge\Symfony\Validator\EventListener
 */
final class ValidationExceptionListener
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Returns a list of violations normalized in the Hydra format.
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        if (!$exception instanceof ValidationException) {
            return;
        }

        $event->setResponse(new Response(
                $this->serializer->serialize($exception->getConstraintViolationList(), 'json'),
                Response::HTTP_BAD_REQUEST,
                [
                    'Content-Type' => sprintf('%s; charset=utf-8', 'application/problem+json'),
                    'X-Content-Type-Options' => 'nosniff',
                    'X-Frame-Options' => 'deny',
                ]
        ));
    }
}
