<?php

namespace GI\RestResourceBundle\Exception;

/**
 * Class RuntimeException
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Exception
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface
{

}
