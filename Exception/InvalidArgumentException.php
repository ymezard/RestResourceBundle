<?php

namespace GI\RestResourceBundle\Exception;

/**
 * Class InvalidArgumentException
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Exception
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{

}
