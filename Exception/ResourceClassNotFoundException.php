<?php

namespace GI\RestResourceBundle\Exception;


class ResourceClassNotFoundException extends \Exception implements ExceptionInterface
{

}