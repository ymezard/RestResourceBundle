<?php

namespace GI\RestResourceBundle\Annotation;

/**
 * Class RestResource
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package GI\RestResourceBundle\Annotation
 *
 * @Annotation
 * @Target({"CLASS"})
 */
final class RestResource
{

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $manager;

    /**
     * @var array
     */
    public $collectionOperations;

    /**
     * @var array
     */
    public $itemOperations;

    /**
     * @var array
     */
    public $attributes = [];
}
