<?php

namespace GI\RestResourceBundle;

use GI\RestResourceBundle\Manager\ResourceManagerInterface;


/**
 * Class ResourceEvents
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle
 */
final class ResourceEvents
{
    const PRE_VALIDATE = 'pre_validate';
    const POST_VALIDATE = 'post_validate';

    const PRE_CREATE = 'pre_create';
    const POST_CREATE = 'post_create';

    const PRE_UPDATE = 'pre_update';
    const POST_UPDATE = 'post_update';

    /**
     * @param string $manager
     * @param string $eventName
     *
     * @return string
     */
    public static function build($resourceName, $eventName)
    {
        return sprintf('%s.%s', $resourceName, $eventName);
    }
}