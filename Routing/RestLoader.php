<?php

namespace GI\RestResourceBundle\Routing;

use Doctrine\Common\Inflector\Inflector;
use RestBundle\Exception\InvalidArgumentException;
use RestBundle\Exception\InvalidResourceException;
use GI\RestResourceBundle\Loader\ResourceMetadata;
use GI\RestResourceBundle\Loader\ResourcesLoader;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceCollectionFactory;
use GI\RestResourceBundle\Metadata\Factory\AnnotationRestResourceMetadataFactory;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RestLoader
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Routing
 */
class RestLoader extends Loader
{

    const ROUTE_NAME_PREFIX = 'api_';
    const DEFAULT_ACTION_PATTERN = 'rest.controller.default:';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var AnnotationRestResourceCollectionFactory
     */
    protected $restResourceCollectionFactory;

    /**
     * @var AnnotationRestResourceMetadataFactory
     */
    protected $restResourceMetadataFactory;

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * RestLoader constructor.
     *
     * @param ContainerInterface $container
     * @param ResourcesLoader    $resourcesLoader
     */
    public function __construct(
        ContainerInterface $container,
            AnnotationRestResourceCollectionFactory $restResourceCollectionFactory,
            AnnotationRestResourceMetadataFactory $restResourceMetadataFactory)
    {
        $this->container = $container;
        $this->restResourceCollectionFactory = $restResourceCollectionFactory;
        $this->restResourceMetadataFactory = $restResourceMetadataFactory;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "extra" loader twice');
        }

        $routeCollection = new RouteCollection();

        /** @var ResourceMetadata $resource */
        foreach ($this->restResourceCollectionFactory->create() as $resourceClass) {
            $resourceMetadata = $this->restResourceMetadataFactory->create($resourceClass);
            $resourceName = $resourceMetadata->getName();

            if (null === $resourceName) {
                throw new InvalidResourceException(sprintf('Resource %s has no short name defined.', $resourceClass));
            }

            if (($collectionOperations = $resourceMetadata->getCollectionOperations()) !== null) {
                foreach ($collectionOperations as $operationName => $operation) {
                    $this->addRoute(
                        $routeCollection,
                        $resourceClass,
                        $operationName,
                        $operation,
                        $resourceName,
                        true
                    );
                }
            }

            if (($itemOperations = $resourceMetadata->getItemOperations()) !== null) {
                foreach ($itemOperations as $operationName => $operation) {
                    $this->addRoute(
                        $routeCollection,
                        $resourceClass,
                        $operationName,
                        $operation,
                        $resourceName,
                        false
                    );
                }
            }
        }

        $this->loaded = true;

        return $routeCollection;
    }

    public function supports($resource, $type = null)
    {
        return $type === 'resource';
    }

    private function addRoute(
        RouteCollection $routeCollection,
        string $resourceClass,
        string $operationName,
        array $operation,
        string $resourceName,
        bool $collection
    ) {

        if (isset($operation['route_name'])) {
            return;
        }

        if (!isset($operation['method'])) {
            throw new \RuntimeException('Either a "route_name" or a "method" operation attribute must exist.');
        }

        $controller = $operation['controller'] ?? null;
        $collectionType = $collection ? 'collection' : 'item';

        if ($controller === null) {
            if ($collection && strtolower($operation['method']) == 'get') {
                $actionName = 'cgetAction';
            } elseif (in_array(strtolower($operation['method']), ['get', 'post', 'put', 'patch', 'delete'])) {
                $actionName = strtolower($operation['method']).'Action';
            } else {
                throw new InvalidArgumentException(sprintf(
                    'Operation %s for resource %s is not available',
                    $operation['method'],
                    $resourceName
                ));
            }

            $controller = self::DEFAULT_ACTION_PATTERN . $actionName;
        }

        $actionName = sprintf('%s_%s', strtolower($operation['method']), $collectionType);
        if ($operationName !== strtolower($operation['method'])) {
            $actionName = sprintf('%s_%s', $operationName, $collectionType);
        }

        $resourceRouteName = Inflector::pluralize(Inflector::tableize($resourceName));
        $routeName = sprintf('api_%s_%s', $resourceRouteName, $actionName);

        if (isset($operation['path'])) {
            $path = $operation['path'];
        } else {
            $path = '/' . $resourceRouteName;
            if (!$collection) {
                $path .= '/{id}';
            }
        }

        $requirements = [];
        if (isset($operation['requirements']) && is_array($operation['requirements'])) {
            $requirements = $operation['requirements'];
        }

        $route = new Route(
            $path,
            [
                '_controller' => $controller,
                '_format' => 'json',
                '_rest_resource_class' => $resourceClass,
                '_rest_resource_name' => $resourceName,
                sprintf('_rest_%s_operation_name', $collection ? 'collection' : 'item') => $operationName
            ],
            $requirements, // requirements
            [], // options
            '', // host
            [], // schemes
            [$operation['method']] // $methods
        );

        $routeCollection->add($routeName, $route);
    }

    private function getControllerLocator($resource)
    {
        $class = null;
        $prefix = null;

        if ($this->container->has($resource)) {
            $prefix = $resource.':';
            //$useScope = method_exists($this->container, 'enterScope') && $this->container->hasScope('request');

            $class = get_class($this->container->get($resource));
        }

        if (empty($class)) {
            throw new \InvalidArgumentException(sprintf(
                'Class could not be determined for Controller identified by "%s".',
                $resource
            ));
        }

        return [$prefix, $class];
    }
}
