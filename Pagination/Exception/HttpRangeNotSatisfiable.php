<?php

namespace GI\RestResourceBundle\Pagination\Exception;


/**
 * Class HttpRangeNotSatisfiable
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Pagination\Exception
 */
class HttpRangeNotSatisfiable extends \OutOfRangeException
{

    /**
     * HttpRangeNotSatisfiable constructor.
     */
    public function __construct($message = 'Range Not Satisfiable')
    {
        parent::__construct($message);
    }
}