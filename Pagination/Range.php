<?php

namespace GI\RestResourceBundle\Pagination;


/**
 * Class Range
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Pagination
 */
class Range
{

    /**
     * @var integer
     */
    protected $start;

    /**
     * @var integer
     */
    protected $end;

    /**
     * @var integer
     */
    protected $maxResults;

    /**
     * Range constructor.
     *
     * @param int $start
     * @param int $end
     * @param int $maxResults
     */
    public function __construct($start, $end, $maxResults)
    {
        $this->start = $start;
        $this->end = $end;
        $this->maxResults = $maxResults;
    }

    /**
     * @return int
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param int $start
     *
     * @return Range
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return int
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param int $end
     *
     * @return Range
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxResults()
    {
        return $this->maxResults;
    }

    /**
     * @param int $maxResults
     *
     * @return Range
     */
    public function setMaxResults($maxResults)
    {
        $this->maxResults = $maxResults;

        return $this;
    }

    public function getCurrentPage()
    {
        $countResults = ($this->getEnd()-$this->getStart()) + 1;
        $offset = $countResults + $this->getStart();

        return ceil($offset / $countResults);
    }
}
