<?php

namespace GI\RestResourceBundle\Security\Domain;

/**
 * Class ResourcePermission
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Security\Domain
 */
final class ResourcePermission
{

    const CREATE    = 'create';
    const LIST      = 'list';
    const VIEW      = 'view';
    const EDIT      = 'edit';
    const DELETE    = 'delete';

    private function __construct()
    {
    }
}
