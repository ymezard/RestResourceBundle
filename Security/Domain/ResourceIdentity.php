<?php

namespace GI\RestResourceBundle\Security\Domain;

/**
 * Class ResourceIdentity
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Security\Domain
 */
final class ResourceIdentity
{

    const IDENTIFIER_COLLECTION = 'COLLECTION';
    const IDENTIFIER_NEW = 'NEW';

    /**
     * @var mixed
     */
    private $subject;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var string
     */
    private $operation;

    /**
     * ResourceIdentity constructor.
     *
     * @param mixed  $subject
     * @param string $identifier
     * @param string $operation
     */
    public function __construct($subject, $identifier, $operation = null)
    {
        $this->subject = $subject;
        $this->identifier = $identifier;
        $this->operation = $operation;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        if (is_object($this->subject)) {
            return sprintf('ResourceIdentity(%s, %s)', get_class($this->subject), $this->identifier);
        }

        return sprintf('ResourceIdentity(%s, %s)', $this->subject, $this->identifier);
    }
}
