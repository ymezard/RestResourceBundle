<?php

namespace GI\RestResourceBundle\Util;

use GI\RestResourceBundle\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestAttributesExtractor
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Util
 */
class RequestAttributesExtractor
{

    public static function extractAttributes(Request $request)
    {
        if (!$request->attributes->has('_rest_resource_class')) {
            throw new RuntimeException('The request is not a rest resource');
        }

        $result = [
            'resource_class' => $request->attributes->get('_rest_resource_class'),
            'resource_name' => $request->attributes->get('_rest_resource_name')
        ];

        $collectionOperationName = $request->attributes->get('_rest_collection_operation_name');
        $itemOperationName = $request->attributes->get('_rest_item_operation_name');

        if ($collectionOperationName) {
            $result['collection_operation_name'] = $collectionOperationName;
        } elseif ($itemOperationName) {
            $result['item_operation_name'] = $itemOperationName;
        } else {
            throw new RuntimeException(
                'One of the request attribute "_api_collection_operation_name" or 
                "_rest_item_operation_name" must be defined.'
            );
        }

        return $result;
    }
}
