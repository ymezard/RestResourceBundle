<?php

namespace GI\RestResourceBundle\Metadata;

use Traversable;

/**
 * Class RestResourceCollection
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Metadata
 */
class RestResourceCollection implements \IteratorAggregate, \Countable
{
    /**
     * @var array
     */
    private $classes;

    /**
     * RestResourceCollection constructor.
     *
     * @param $classes
     */
    public function __construct(array $classes = [])
    {
        $this->classes = $classes;
    }


    public function getIterator()
    {
        return new \ArrayIterator($this->classes);
    }


    public function count()
    {
        return count($this->classes);
    }
}
