<?php

namespace GI\RestResourceBundle\Metadata\Factory;

use Doctrine\Common\Annotations\Reader;
use RestBundle\Annotation\RestResource;
use GI\RestResourceBundle\Metadata\RestResourceCollection;

/**
 * Class AnnotationRestResourceCollectionFactory
 *
 * @author  Marc Jacquier <marc@getinov.com>
 * @package RestBundle\Metadata\Factory
 */
class AnnotationRestResourceCollectionFactory
{

    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var array
     */
    private $classes;

    /**
     * @var RestResourceCollection
     */
    protected $resources;

    /**
     * AnnotationRestResourceCollectionFactory constructor.
     *
     * @param Reader $reader
     * @param array  $paths
     */
    public function __construct(Reader $reader, array $classes)
    {
        $this->reader = $reader;
        $this->classes = $classes;
    }

    public function create()
    {
        if (null !== $this->resources) {
            return $this->resources;
        }

        $resources = [];
        foreach ($this->classes as $className) {
            if (!class_exists($className)) {
                continue;
            }

            $reflectionClass = new \ReflectionClass($className);
            if ($this->reader->getClassAnnotation($reflectionClass, RestResource::class)) {
                $resources[$className] = true;
            }
        }

        return $this->resources =  new RestResourceCollection(array_keys($resources));
    }
}
