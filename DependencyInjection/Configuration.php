<?php

namespace GI\RestResourceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('rest');

        $rootNode
            ->children()
                ->arrayNode('bundles')
                    ->prototype('scalar')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

}